# ESP32 ADC Adjustment - README #
---

### Overview ###

The **ESP32_ADC** function (Arduino IDE sketch) will try to adjust the input values from the ADCs (Analog to Digital Converters) of an ESP32 to make them more linear and therefore get more exact measurement values to work with. This function uses a  [**Polynomial Regression**](https://en.wikipedia.org/wiki/Polynomial_regression) formula resulting from values of a [**Polynomial Regression Data Fit**](https://arachnoid.com/polysolve/) to get the most accurate values. More information and examples can be found on the [**Arachnoid Website**](https://arachnoid.com/sage/polynomial.html). If the function **analogReadAdjusted()** is called, a number of measurements will be taken to return the average value and get a more stable result. In the function can be defined how many measurements should be made and the waiting time between them. The ADCs of the ESP32 do not return usable values from an input voltage below 0.3 volts and above 3.1 volts. Therefore the usable range is from about 0.3 - 3.1 volts. The function can not provide better results for the very low or very high voltage range because the ADCs return also 0 if the voltage is 0.1 volts and 4095 if the voltage is 3.2 volts (examples). This is mostly a hardware problem and can not be solved by software.

### Screenshots ###

![ESP32_ADC - Result](development/readme/esp32adc1.png "ESP32_ADC - Result")

![ESP32_ADC - Test Measurements](development/readme/esp32adc2.png "ESP32_ADC - Test Measurements")

### Setup - Function Test ###

* Copy the file **ESP32_ADC.ino** to your computer.
* Open the file in the Arduino IDE or in your favorite editor.
* Specify the correct pin number of the analog pin you use (constant **pinNumber** on line 8).
* Save the file and upload it to your ESP32.
* Open the serial monitor to check the output.
* Your analog pin should receive a voltage between 0 and 3.3 volts.
* The usable range for the data is between 0.3 and 3.1 volts.

### Setup - Function Usage ###

* Copy the file **ESP32_ADC.ino** to your computer.
* Open the file in the Arduino IDE or in your favorite editor.
* Copy the function **analogReadAdjusted()** at the end of the code.
* Place this function into the code of your project.
* Specify the desired number of loops for the measurements (constant **loops**).
* Specify the waiting time between the loops (constant **loopDelay**).
* Now instead of calling the standard **analogRead()** function use the new one.
* Call the function like in the following example: **analogReadAdjusted(pinNumber)**.
* The returned average value will be created with different measurements and is adjusted.

### Adjustments to the Polynomial Regression Formula ###

* Maybe not all ESP32 ADCs are equal and therefore the formula does not match your device.
* You can create your own formula and replace the necessary factors of the function.
* Run the sketch **ESP32_ADC.ino** and open the serial monitor.
* Apply all the different voltages 0.1 / 0.2 .... up to 3.3 volts to your analog input pin.
* Write down all the **Input** values from the serial monitor output.
* You should get 34 values between 0 and 4095.
* Open the spreadsheet **ESP32_Mesaurements.ods** to insert the new data in the grey/orange column.
* Then copy the data from the grey/orange column and the green column to the right (**B2:C35**).
* Go to the [**Polynomial Regression Data Fit Website**](https://arachnoid.com/polysolve/).
* Scroll a little bit down to the **Data Entry Area** and delete all the existing data in the textarea.
* Paste your data into the textarea - You can see your data in the chart below.
* Below the chart you have to increase the **Degree** from 2 to 11.
* In the **Results Area** you will get 12 new factors (coefficients).
* Copy them and replace the existing factors in the function **analogReadAdjusted()**.
* Each number on the line **const double f1 = ...** up to **const double f12 = ...** has to be replaced.
* Save the changes and test the function - Now it should match your ESP32!

### Support ###

This is a free sketch and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **ESP32_ADC** sketch is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
